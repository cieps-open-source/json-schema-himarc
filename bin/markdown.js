#!/usr/bin/env node

const process = require("node:process");
const path = require("path");
const fs = require("fs");
const Promise = require("bluebird");
const writeFileAsync = Promise.promisify(fs.writeFile);
const schema = require("../dist/himarc-register.schema.json"); // register ou work ?

/**
 * Generate Markdown file title
 *
 * Args:
 *     - tag: Tag number
 *     - field: field info from schema
 **/
function generateFieldTitle(tag, field) {
  let title = `# ${tag} - ${field.title}`;
  title = (field.ISSNProfile) ? `${title} [${field.ISSNProfile}]` : `${title}`;
  title = (field.isRepeatable) ? `${title} [R]` : `${title} [NR]`;

  return title;
}

/**
 * Generate markdown file content according to datafield tag
 *
 * Args:
 *     - tag : Tag value to extract info from schema
 *
 * Returns the file content such as
 * ```
 * # 015 - National Bibliography Number [R]
 *
 * |=Indicator1 |=Label
 * | \ | Undefined (default)
 *
 * |=Indicator2 |=Label
 * | \ | Undefined (default)
 *
 * |=Subfield |=Label |=Mandatory |=Repeatable
 * | 2 | Source | false | false
 * | 6 | Linkage | false | false
 * | 8 | Field link and sequence number | false | true
 * ```
 **/
function generateDataFieldMarkdown(tag) {
  let tagValue = schema.properties.fields.properties[tag];
  tagValue = tagValue.type === "array" ? {
    ...tagValue,
    ...tagValue.items
  } : tagValue;

  const content = [generateFieldTitle(tag, tagValue), ""];

  // indicators
  content.push("## Indicators", "");
  ["indicator1", "indicator2"].forEach((key) => {
    const keyName = key.charAt(0).toUpperCase() + key.slice(1);
    content.push(`|=${keyName} |=Label`);
    Object.entries(tagValue.properties[key].code || {}).forEach(([code, values]) => {
      if (tagValue.properties[key].defaultValue === code) {
        content.push(`| ${code} | ${values.title} (default)`);
      } else {
        content.push(`| ${code} | ${values.title}`);
      }
    });
    content.push("");
  });

  // subfields
  content.push("## Subfields", "");
  handleSubFieldsMarkdown(content, tagValue.properties.subFields.items.defaultProperties, tagValue.properties.subFields.items.properties);
  handleSubFieldsMarkdown(content, tagValue.properties.subFields.items.defaultProperties, tagValue.properties.subFields.items.patternProperties);

  return content.join("\n");
}



/**
 * handling subfields for markdown.
 *
 * Args:
 *    - file content
 *    - defaults properties
 *    - props sush as properties or patternProperties
 *
 **/
function handleSubFieldsMarkdown(content, defaultProps, props) {
  if (props) {
    content.push("|=Subfield |=Label |=Mandatory |=Repeatable");
    Object.entries(props).forEach((
      [code, values]
    ) => {
      const isDefault = defaultProps.includes(code);
      const title = (isDefault) ? `${values.title} (default)` : values.title;
      const isMandatory = values.issnProfile || "";
      const isRepeatable = (values.isRepeatable) ? "yes" : "no";

      content.push(
        `| ${code.replace("|", "&#124;")} | ${title} | ${isMandatory} | ${isRepeatable}`
      );
    });
    content.push("");
  }
}

/**
 * Extract position definitions from schema and generate markdown table.
 *
 * Args:
 *    - lst: JSON properties object containing the codes definition
 *
 **/
function extractPositionDefinitions(lst) {
  const sortPositions = (obj) => {
    return Object.entries(obj).sort(([posA], [posB]) => {
      if (posA.length > 2) posA = posA.slice(0, 2);
      if (posB.length > 2) posB = posB.slice(0, 2);
      return parseInt(posA) - parseInt(posB);
    });
  };

  return sortPositions(lst).reduce((result, [pos, value]) => {
    if (value.codes) {
      const title = value.title;
      const details = [];
      Object.entries(value.codes).forEach(([code, codeValue]) => {
        details.push(
          `| | | ${code.replace("|", "&#124;")} | ${codeValue.title}`
        );
      });

      result.push(
        `|=${pos} |=${title} |=Code |=Label`,
        `${details.join('\n')}`
      );
    }

    return result;
  }, [`|=Position|=Label|=(% colspan="2" %)Details`]);
}

/**
 * Generate markdown file content according to controlfield tag.
 * Only 006/007 and 008 field positions definitions are extracted
 *
 *
 * Args:
 *     - tag : Tag value to extract info from schema
 *
 * Returns the file content such as
 * ```
 * # 006 - Fixed-Length Data Elements-Additional Material Characteristics [optional] [R]
 *
 * **Books**
 * |=Position|=Label|=(% colspan="2" %)Details
 * |=01-04 |=Illustrations |=Code |=Label
 * | | |   | No illustrations
 * | | | a | Illustrations
 * | | | b | Maps
 * | | | c | Portraits
 * | | | d | Charts
 * | | | e | Plans
 * | | | f | Plates
 * | | | g | Music
 * | | | h | Facsimiles
 * | | | i | Coats of arms
 * | | | j | Genealogical tables
 * | | | k | Forms
 * | | | l | Samples
 * | | | m | Phonodisc, phonowire, etc.
 * | | | o | Photographs
 * | | | p | Illuminations
 * | | | &#124; | No attempt to code
 * |=05 |=Target audience |=Code |=Label
 * | | |   | Unknown or unspecified
 * | | | a | Preschool
 * | | | b | Primary
 * | | | c | Pre-adolescent
 * | | | d | Adolescent
 * | | | e | Adult
 * | | | f | Specialized
 * | | | g | General
 * | | | j | Juvenile
 * | | | &#124; | No attempt to code
 * ```
 **/
function generateControlFieldMarkdown(tag) {
  let tagValue = schema.properties.fields.properties[tag];
  tagValue = tagValue.type === "array" ? {
    ...tagValue,
    ...tagValue.items
  } : tagValue;

  const content = [generateFieldTitle(tag, tagValue), ""];

  if (["006", "008"].includes(tag)) {
    Object.entries(tagValue.definitions).forEach(([name, fieldType]) => {
      content.push(
        `## ${name}`,
        "",
        ...extractPositionDefinitions(fieldType.properties.positions.properties),
        ""
      );
    });
  } else if (tag === "007") {
    Object.values(tagValue.definitions).forEach((item) => {
      const code = item.properties["00"].const;
      content.push(
        `## ${item.properties["00"].codes[code].title}`,
        "",
        ...extractPositionDefinitions(item.properties),
        ""
      );
    });
  }

  return content.join("\n");
}

/**
 * Generate markdown file content concerning the leader positions.
 *
 * Returns the file content such as
 * ```
 * # LDR - Leader [mandatory] [NR]
 *
 * |=Position|=Label|=(% colspan="2" %)Details
 * |=18 |=Descriptive cataloging form |=Code |=Label
 * | | |   | Non-ISBD
 * | | | a | AACR 2
 * | | | c | ISBD punctuation omitted
 * | | | i | ISBD punctuation included
 * | | | n | Non-ISBD punctuation omitted
 * | | | u | Unknown
 * ```
**/
function generateLeaderMarkdown() {
  const content = ["# LDR - Leader [mandatory] [NR]", ""];

  content.push(
    ...extractPositionDefinitions(
      schema.properties.fields.properties.LDR.properties.positions.properties
    ),
    ""
  );

  return content.join("\n");
}

/** CLI entrypoint **/
async function main() {
  try {
    for (const tag of Object.keys(schema.properties.fields.properties)) {
      let content;
      const output = path.join(__dirname, "../dist/markdown", `${tag}.md`);

      if (isNaN(tag)) { // LDR
        content = generateLeaderMarkdown();
      } else if (parseInt(tag) < 10) { // control fields
        content = generateControlFieldMarkdown(tag);
      } else { // data fields
        content = generateDataFieldMarkdown(tag);
      }

      await writeFileAsync(output, content);
    }
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

main()
