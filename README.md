 [![pipeline status](https://gitlab.com/cieps-open-source/json-schema-himarc/badges/master/pipeline.svg)](https://gitlab.com/cieps-open-source/json-schema-himarc/-/commits/master) [![Latest Release](https://gitlab.com/cieps-open-source/json-schema-himarc/-/badges/release.svg)](https://gitlab.com/cieps-open-source/json-schema-himarc/-/releases)

# json-schema-himarc

JSON Schema for himarc : https://cieps-open-source.gitlab.io/json-schema-himarc/

## Install

npm packages are hosted in GitLab.

`npm install https://gitlab.com/cieps-open-source/json-schema-himarc/-/releases/X.Y.Z/downloads/json-schema-himarc-X.Y.Z`

Where `X.Y.Z` is [the version](https://gitlab.com/cieps-open-source/json-schema-himarc/-/releases)
you want to install.

## Build

- `npm run bundle` : generate himarc-register.schema.json & himarc-work.schema.json in /dist
- `npm run build` : generate the documentation in /dist
- `npm run build_markdown` : generate markdown documentation in /dist/markdown

## Dev

- `npm run test` : run tests
- `npm run serve` : run documentation's devserver
