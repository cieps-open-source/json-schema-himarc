import Vue from 'vue';
import Buefy from 'buefy';
import '@mdi/font/css/materialdesignicons.min.css';
import App from './App.vue';

Vue.prototype.$ISSN_PROFILE_LDR_007_POSITION_CODES_RE = /(i|s)/;
Vue.prototype.$ISSN_PROFILE_006_TITLES_RE = /^((?!(Continuing Resources)).)*$/;
Vue.prototype.$ISSN_PROFILE_007_KEYS_RE = /0(0|1)/;
Vue.prototype.$ISSN_PROFILE_008_TITLES_RE = /(All Materials|Continuing Resources)/;

Vue.use(Buefy);

Vue.config.productionTip = false;

new Vue({
  render: function (h) { return h(App); }
}).$mount('#app');
