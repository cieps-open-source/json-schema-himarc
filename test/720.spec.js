/* eslint-env mocha */
/* eslint-disable no-unused-expressions */
'use strict';

const { expect } = require('chai');
const Ajv = require('ajv').default;
const schemaHelper = require('../src');
const ajv = new Ajv({
    allErrors: true,
    strict: false
});


describe('Added Entry--meeting Name (720) schema', () => {
  let validate;

  beforeEach(() => {
    validate = ajv.compile(schemaHelper.field_720_register)
  });

  [
    { ind1: "\\", ind2: "\\"},
    { ind1: "1", ind2: "\\"},
    { ind1: "2", ind2: "\\"},
  ].forEach((params) => {
    it(`should validate (${params.ind1} is a valid indicator1 with an ISNI)`, () => {
      const data = [{
        indicator1: params.ind1,
        indicator2: params.ind2,
        subFields: [
          {
            ['a']: 'Côte d\'Ivoire', ['0']: '(discogs)a312098'
          }
        ]
      }];
      const valid = validate(data);
      if (validate.errors) console.dir(validate.errors, { depth: 8 });
      expect(valid).to.be.true;
    });
  });
});
