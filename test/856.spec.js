'use strict';

const { expect } = require('chai');
const Ajv = require('ajv').default;
const schemaHelper = require('../src');
const ajv = new Ajv({
    allErrors: true,
    strict: false
});


describe('Electronic Location and Access (856) schema', () => {
  let validate;

  beforeEach(() => {
    validate = ajv.compile(schemaHelper.field_856);
  });

  [
    { ind1: '4', ind2: '2', subfieldCode: 'u' },
    { ind1: '4', ind2: '2', subfieldCode: 'h' },
    { ind1: '4', ind2: '2', subfieldCode: 'g' },
  ].forEach((params) => {
    it(`should validate with a valid subfield ${params.subfieldCode}`, () => {
      const data = [{
        indicator1: params.ind1,
        indicator2: params.ind2,
        subFields: [
          {
            [params.subfieldCode]: 'https://www.loc.gov/marc/'
          }
        ]
      }];
      const valid = validate(data);
      if (validate.errors) console.dir(validate.errors, { depth: 8 });
      expect(valid).to.be.true;
    });
  });
});
