'use strict';

const { expect } = require('chai');
const Ajv = require('ajv').default;
const schemaHelper = require('../src');
const ajv = new Ajv({
    allErrors: true,
    strict: false
});


describe('Electronic Archive Location and Access (857) schema', () => {
  let validate;

  beforeEach(() => {
    validate = ajv.compile(schemaHelper.field_857);
  });

  [
    { ind1: '4', ind2: '0', subfieldCode: 'u' },
    { ind1: '4', ind2: '0', subfieldCode: 'h' },
    { ind1: '4', ind2: '0', subfieldCode: 'g' },
  ].forEach((params) => {
    it(`should validate with both subfield b and subfield ${params.subfieldCode}`, () => {
      const data = [{
        indicator1: params.ind1,
        indicator2: params.ind2,
        subFields: [
          {
            ['b']: 'ARCHIVING AGENCY', [params.subfieldCode]: 'https://www.loc.gov/marc/'
          }
        ]
      }];
      const valid = validate(data);
      if (validate.errors) console.dir(validate.errors, { depth: 8 });
      expect(valid).to.be.true;
    });
  });

  it('should not validate with a missing subfield b', () => {
      const data = [{
      indicator1: '4',
      indicator2: '0',
      subFields: [
          {
            'u': 'https://www.loc.gov/marc/'
          }
        ]
      }];
      const valid = validate(data);
      expect(valid).to.be.false;
    });

});

