/* eslint-env mocha */
/* eslint-disable no-unused-expressions */
'use strict';

const { expect } = require('chai');
const Ajv = require('ajv').default;
const schemaHelper = require('../src');

const ajv = new Ajv({
  allErrors: true,
  strict: false
});

describe('Copyright or Legal Deposit Number (017)', () => {
  it('should validate', () => {
    const data = [
      {
        indicator1: "\\",
        indicator2: "\\",
        subFields: [
          {a: "PA 1-060-815"},
          {b: "U.S. Copyright Office"}
        ]
      }
    ];
    const validate = ajv.compile(schemaHelper.field_017);
    const valid = validate(data);
    if (validate.errors) console.dir(validate.errors, { depth: 8 });
    expect(valid).to.be.true;
  });
  [
    [
      {
        indicator1: "1",
        indicator2: "\\",
        subFields: [
          {a: "PA 1-060-815"},
          {b: "U.S. Copyright Office"},
        ]
      }
    ], [
      {
        indicator1: "\\",
        indicator2: "1",
        subFields: [
          {a: "PA 1-060-815"},
          {b: "U.S. Copyright Office"}
        ]
      }
    ], [
      {
        indicator1: "\\",
        indicator2: "\\",
        subFields: [
          {a: "PA 1-060-815"},
          {e: "U.S. Copyright Office"}
        ]
      }
    ]
  ].forEach((data) => {
    it('shouldn\'t validate with an unauthorized values', () => {
      const validate = ajv.compile(schemaHelper.field_017);
      const valid = validate(data);
      expect(valid).to.be.false;
    })
  });
});
