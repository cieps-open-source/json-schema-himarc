/* eslint-env mocha */
/* eslint-disable no-unused-expressions */
'use strict';

const { expect } = require('chai');
const Ajv = require('ajv').default;
const schemaHelper = require('../src');

const ajv = new Ajv({
  allErrors: true,
  strict: false
});

describe('Physical Description (007) schema', () => {
  it('should validate ta', () => {
    const data = [{
      positions: {
        '00': 't',
        '01': 'a'
      }
    }];
    const validate = ajv.compile(schemaHelper.field_007);
    const valid = validate(data);
    if (validate.errors) console.dir(validate.errors, { depth: 8 });
    expect(valid).to.be.true;
  });

  it('shouldn\'t validate xa', () => {
    const data = [{
      positions: {
        '00': 'x',
        '01': 'a'
      }
    }];
    const validate = ajv.compile(schemaHelper.field_007);
    const valid = validate(data);
    if (validate.errors) {
      expect(validate.errors.some(error => error.message === 'should be equal to one of the allowed values')).to.be.true;
    }
    expect(valid).to.be.false;
  });

  [
    ['|||', true],
    ['123', true],
    ['45|', false],
    ['abc', false],
    ['2--', true],
    ['32-', true],
    ['---', true],
    ['2-3', false],
  ].forEach(([position, valid]) => {
    it(`validate ${position} positions 06 to 08`, () => {
      const data = [{
        positions: {
          '00': 'h',
          '06-08': position,
        }
      }];
      const validate = ajv.compile(schemaHelper.field_007);
      const result = validate(data);
      expect(result).to.deep.equal(valid);
    });
  });

  it('should validate ca|bee|||ancan', () => {
    const data = [{
      positions: {
        '00': 'c',
        '01': 'a',
        '02': '|',
        '03': 'b',
        '04': 'e',
        '05': 'u',
        '06-08': '|||',
        '09': 'a',
        '10': 'n',
        '11': 'c',
        '12': 'a',
        '13': 'n',
      }
    }];
    const validate = ajv.compile(schemaHelper.field_007);
    const valid = validate(data);
    if (validate.errors) console.dir(validate.errors, { depth: 8 });
    expect(valid).to.be.true;
  });

  it('shouldn\'t validate caz', () => {
    const data = [{
      positions: {
        '00': 'c',
        '01': 'a',
        '02': 'z'
      }
    }];
    const validate = ajv.compile(schemaHelper.field_007);
    const valid = validate(data);
    if (validate.errors) console.dir(validate.errors, { depth: 8 });
    expect(valid).to.be.false;
  });
});
