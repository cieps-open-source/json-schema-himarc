/* eslint-env mocha */
/* eslint-disable no-unused-expressions */
'use strict';

const { expect } = require('chai');
const Ajv = require('ajv').default;
const schemaHelper = require('../src');

const ajv = new Ajv({
    allErrors: true,
    strict: false
});

describe('Cluster ISSN (023) schema', () => {

    let indicator1, indicator2, subfields, data;

    beforeEach(() => {
        indicator1 = '0';
        indicator2 = '\\';
        subfields = [
            {
                a: '0028-0836'
            },
        ];
        data = [{
            indicator1: indicator1,
            indicator2: indicator2,
            subFields: subfields
        }]
    });
    it('must be valid when all required fields are set', () => {
        const validate = ajv.compile(schemaHelper.field_023);
        const valid = validate(data);
        if (validate.errors) console.dir(validate.errors, {
            depth: 8
        });
        expect(valid).to.be.true;
    });
    it('must not allow a whitespace-only subfield value', () => {
        data[0].subFields.push({ "0": " " });
        const validate = ajv.compile(schemaHelper.field_023);
        const valid = validate(data);
        expect(valid).to.be.false;
        expect(validate.errors[0].message).to.equal('should match pattern "^(?!\\s*$).+"');
    });
    it('must not allow a missing required property', () => {
        data[0].subFields.pop();
        const validate = ajv.compile(schemaHelper.field_023);
        const valid = validate(data);
        expect(valid).to.be.false;
        expect(validate.errors[0].message).to.equal('should contain at least 1 valid item(s)');
    });
    it('must not allow an extra property', () => {
        data[0].subFields.push({ x: 'X' })
        const validate = ajv.compile(schemaHelper.field_023);
        const valid = validate(data);
        expect(valid).to.be.false;
        expect(validate.errors[0].message).to.equal('should NOT have additional properties');
    });
    it('must be valid when indicator1 is set to 1 and $a is absent but $z is present', () => {
        data[0].indicator1 = '1';
        // Rename key
        delete Object.assign(data[0].subFields, {['z']: data[0].subFields['a']})['a'];
        const validate = ajv.compile(schemaHelper.field_023);
        const valid = validate(data);
        expect(valid).to.be.true;
    });
});
