/* eslint-env mocha */
/* eslint-disable no-unused-expressions */
'use strict';

const { expect } = require('chai');
const Ajv = require('ajv').default;
const schemaHelper = require('../src');
const ajv = new Ajv({
    allErrors: true,
    strict: false
});


describe('Special Coded Dates (046) schema', () => {
  let validate;

  beforeEach(() => {
    validate = ajv.compile(schemaHelper.field_046)
  });

  [
    { ind1: "\\", ind2: "\\", subfieldA: 'k'},
    { ind1: "1", ind2: "\\", subfieldA: 'q'},
    { ind1: "2", ind2: "\\", subfieldA: 'x'},
    { ind1: "3", ind2: "\\", subfieldA: 'r'},
  ].forEach((params) => {
    it(`should validate (${params.ind1} is a valid indicator1)`, () => {
      const data = [{
        indicator1: params.ind1,
        indicator2: params.ind2,
        subFields: [
          {
            a: params.subfieldA
          }
        ]
      }];
      const valid = validate(data);
      if (validate.errors) console.dir(validate.errors, { depth: 8 });
      expect(valid).to.be.true;
    });
  });
});
